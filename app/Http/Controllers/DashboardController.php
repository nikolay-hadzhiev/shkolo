<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tab;
use App\Http\Requests\UpdateTabRequest;
use App\Http\Requests\StoreTabRequest;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data['tabs'] = Tab::orderBy('created_at','DESC')
                            ->get();

        return view('pages/dashboard/list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages/dashboard/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTabRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = auth()->user()->id;

        Tab::create($data);
        
        return redirect('dashboard')
                    ->with('status', __('messages.insert_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Tab::findOrFail($id);
        
        return view('pages/dashboard/edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTabRequest $request, $id)
    {
        $data = $request->all();

        Tab::findOrFail($id)->fill($data)->save();
        
        return redirect('dashboard')
                    ->with('status', __('messages.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tab::findOrFail($id)->delete();

        return redirect('dashboard')
                    ->with('status', __('messages.delete_success'));
    }
}
