<?php

namespace App\Http\Helpers;

class Helpers {

	public static function extUrl($url){

		$props = parse_url($url);
		if(!isset($props['scheme'])){
			return '//'.$url;
		}
		return $url;
	}
}