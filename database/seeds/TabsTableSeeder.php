<?php

use Illuminate\Database\Seeder;
use App\Models\Tab;

class TabsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
        	[
                'user_id' => 1,
                'link' => 'www.google.com',
                'color' => 'info',
                'title' => 'Link',
            ],
            [
                'user_id' => 1,
                'link' => 'www.google.com',
                'color' => 'info',
                'title' => 'Link',
            ],
            [
                'user_id' => 1,
                'link' => 'www.google.com',
                'color' => 'info',
                'title' => 'Link',
            ],
            [
                'user_id' => 1,
                'link' => 'www.google.com',
                'color' => 'info',
                'title' => 'Link',
            ],
            [
                'user_id' => 1,
                'link' => 'www.google.com',
                'color' => 'info',
                'title' => 'Link',
            ],
            [
                'user_id' => 1,
                'link' => 'www.google.com',
                'color' => 'info',
                'title' => 'Link',
            ],
            [
                'user_id' => 1,
                'link' => 'www.google.com',
                'color' => 'info',
                'title' => 'Link',
            ],
            [
                'user_id' => 1,
                'link' => 'www.google.com',
                'color' => 'info',
                'title' => 'Link',
            ],
            [
                'user_id' => 1,
                'link' => 'www.google.com',
                'color' => 'info',
                'title' => 'Link',
            ]

        ];

        foreach ($data as $row) {
        	Tab::create($row);
        }
    }
}
