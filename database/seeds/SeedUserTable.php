<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class SeedUserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Lyubomir Vanyov',
            'email' => 'lyubomir.vanyov@shkolo.com',
            'password' => Hash::make('12345678'),
        ]);
    }
}
