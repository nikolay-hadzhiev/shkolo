 <div class="m-form__actions m-form__actions--solid">
    <div class="row">
         <div class="col-lg-6 m--align-left">
            
            <a href="{{ URL::to('/')}}/{{Request::segment(1)}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
                <span>
                    <i class="la la-angle-left v-al-middle"></i>
                    <span>{{ __('controls.back') }}</span>
                </span>
            </a>

        </div>
        <div class="col-lg-6 m--align-right">
            
             <button type="submit" class="btn btn-success m-btn m-btn--custom m-btn--icon m-btn--pill m-btn--air">
                {{ __('controls.save')}}
            </button>
        </div>
       
       
    </div>
</div>