	<footer class="m-grid__item		m-footer ">
		<div class="m-container m-container--fluid m-container--full-height m-page__container">
			<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
				<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
					<span class="m-footer__copyright">
						{{ date('Y')}} &copy; {{ __('info.site_name')}}. {{ __('info.rights')}}
					</span>
				</div>
				<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
		
				</div>
			</div>
		</div>
	</footer>

	<!-- end::Footer -->
	</div>

	<!-- end:: Page -->


	<!-- begin::Scroll Top -->
	<div id="m_scroll_top" class="m-scroll-top">
		<i class="la la-arrow-up"></i>
	</div>

	<!-- end::Scroll Top -->

	<script src="{{ URL::to('/') }}/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/assets/app/js/dashboard.js" type="text/javascript"></script>
	<script type="text/javascript" src="{{ URL::to('/') }}/js/parsley/parsley.min.js"></script>
	<script type="text/javascript" src="{{ URL::to('/') }}/js/parsley/en.js"></script>
	<script src="{{ URL::to('/') }}/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script type="text/javascript" src="{{ URL::to('/') }}/js/bootbox.min.js"></script>
	@yield('scripts')
</body>


