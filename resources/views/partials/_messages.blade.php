
@if($errors->any())
    @foreach($errors->all() as $error)
        <script type="text/javascript">
            toastr.error('{{ $error }}');
        </script>
    @endforeach


@endif

@if(Session::has('status'))
      <script type="text/javascript">
          toastr.success('{{ Session::get('status') }}');
     </script>
@endif