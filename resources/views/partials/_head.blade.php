<head>
	<meta charset="utf-8" />
	<title>{{__('info.site_name')}} - @yield('title')</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Roboto:300,400,500,600,700"]
			},
			active: function () {
				sessionStorage.fonts = true;
			}
		});

	</script>


	<link href="{{ URL::to('/') }}/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{{ URL::to('/') }}/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{{ URL::to('/') }}/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
	<link href="{{ URL::to('/') }}/css/main.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/parsley/parsley.css"/>
	<link rel="shortcut icon" href="{{ URL::to('/') }}/images/system/icon.png" />
	<script src="{{ URL::to('/') }}/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
	<script type="text/javascript">
		
	  toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": true,
	  "progressBar": false,
	  "positionClass": "toast-top-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "2000",
	  "hideDuration": "1000",
	  "timeOut": "5000",
	  "extendedTimeOut": "2000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	};
	</script>
</head>
<!-- begin::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
	<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">