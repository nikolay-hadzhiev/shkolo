@extends('main')

@section('title',__('menu.menu'))

@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">

        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">{{__('menu.dashboard')}}</h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="{{ URL::to('/') }}" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="{{ URL::to('/') }}" class="m-nav__link">
                                <span class="m-nav__link-text">{{__('menu.dashboard')}}</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <span class="m-nav__link-text">{{__('controls.create')}}</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>

                </div>
            </div>
        </div>
        <!-- END: Subheader -->
         <form class="m-form m-form--fit m-form--label-align-right" method="POST"  action="{{ URL::to('/') }}/dashboard/store" data-parsley-validate>
        @csrf 
        <div class="m-content">
             
            <div class="row">

               
                <div class="col-lg-12">

                    <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--mobile m-portlet--body-progress-">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        {{__('content.create_tab')}}
                                    </h3>
                                </div>
                            </div>
                           
                        </div>
                        <div class="m-portlet__body">

                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">{{__('content.title')}}</label>
                                <div class="col-8">
                                    <input class="form-control m-input" type="text"  name="title" required>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-text-input" class="col-2 col-form-label">{{__('content.link')}}</label>
                                <div class="col-8">
                                    <input class="form-control m-input" type="text"  name="link"  data-parsley-type="url">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label for="example-color-input" class="col-2 col-form-label">{{__('content.color')}}</label>
                                <div class="col-8">
                                    <select class="form-control m-input m-input--square" name="color">
                                        <option value="info">{{__('content.colors.blue')}}</option>
                                        <option value="success">{{__('content.colors.green')}}</option>
                                        <option value="danger">{{__('content.colors.red')}}</option>
                                        <option value="warning">{{__('content.colors.yellow')}}</option>
                                        <option value="dark">{{__('content.colors.black')}}</option>
                                        <option value="light">{{__('content.colors.white')}}</option>
                                    </select>
                                </div>
                            </div>

                        

                        </div>
                        @include('partials.components.controls._table_create_buttons')

                    </div>

                    
                </div>

            </div>

             
        </form> 
        
        </div>

    </div>

</div>


@stop

@section('scripts')
 
@stop
