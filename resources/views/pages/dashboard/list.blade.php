@extends('main')

@section('title',__('menu.dashboard'))


@section('stylesheets')

@stop


@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">{{__('menu.dashboard')}}</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="{{ URL::to('/') }}" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                    <li class="m-nav__item">
                        <a href="{{ URL::to('/') }}" class="m-nav__link">
                            <span class="m-nav__link-text">{{__('menu.dashboard')}}</span>
                        </a>
                    </li>
                
                </ul>
            </div>
            <div>
                <a href="{{ URL::to('/') }}/dashboard/create" class="btn btn-success btn-sm m-btn     m-btn m-btn--icon">
                    <span>
                        <i class="la    la-plus"></i>
                        <span>{{__('content.new_tab')}}</span>
                    </span>
                </a>
        
            </div>
            <div>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <div class="m-content">
        
        @foreach($tabs->chunk(3) as $key => $subtabs)
    
        <div class="row">
                @foreach($subtabs as $item)
                <div class="col-lg-4">
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-paper-plane"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    {{$item->title}}
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                
                                <li class="m-portlet__nav-item">
                                    <a href="{{ URL::to('/') }}/dashboard/edit/{{$item->id}}" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la   la-pencil-square"></i></a>
                                </li>
                                <li class="m-portlet__nav-item">
                                    <a href="" data-id="{{$item->id}}" class="btn-delete m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <a href="{{$item->link == '' ? URL::to('/').'/dashboard/edit/'.$item->id : Helper::extUrl($item->link)}}" class="btn btn-{{$item->color}} m-btn m-btn--icon m-btn--pill">
                            <span>
                                <i class="la la-external-link"></i>
                                <span>Visit page</span>
                            </span>
                        </a>
                    </div>
                </div>

            </div>
            @endforeach
        </div>
    @endforeach

    </div>




</div>

</div>

</div>
<form method="POST" id="delete_tab_form">
    @method('DELETE')
    @csrf 
</form>
@stop

@section('scripts')
<script type="text/javascript">
       
      $('body').on('click', '.btn-delete', function () {
          event.preventDefault();
          let id = $(this).attr('data-id');
          bootbox.confirm({
              message: "{{__('messages.confirm_delete')}}",
              buttons: {
                 
                  confirm: {
                      label: '{{__('controls.yes')}}',
                      className: 'btn-success pull-left'
                  }, cancel: {
                      label: '{{__('controls.no')}}',
                      className: 'btn-default pull-right'
                  }
              },
              callback: function (result) {
                  if(result){
                    $('#delete_tab_form').attr('action','{{URL::to('/dashboard/delete/')}}/'+id );    
                    $('#delete_tab_form').submit();
                  }
              }
          });
      
         
      } );
</script>
 
@stop
