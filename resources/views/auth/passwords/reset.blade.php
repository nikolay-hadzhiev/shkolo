<!DOCTYPE html>

<html lang="en">

    <!-- begin::Head -->
   <head>
    <meta charset="utf-8" />
    <title>{{__('info.site_name')}} - {{__('auth.login')}}</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
      });
    </script>

    <link href="{{ URL::to('/') }}/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('/') }}/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('/') }}/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('/') }}/css/main.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{ URL::to('/') }}/images/system/icon.png" />
    <link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/parsley/parsley.css"/>
</head>

    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" >
                <div class="m-grid__item m-grid__item--fluid    m-login__wrapper">
                    <div class="m-login__container">
                        <div class="m-login__logo">
                            <a href="/">
                                <img src="{{ URL::to('/') }}/images/system/parsley.png">
                            </a>
                        </div>
                        @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        <div class="m-login__signin">

                            <div class="m-login__head">
                                <h3 class="m-login__title">{{ __('auth.password_reset') }}</h3>
                            </div>
                            <form class="m-login__form m-form" action="{{ route('password.update') }}" method="POST" data-parsley-validate>
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="email" placeholder="{{ __('auth.email') }}" name="email" autocomplete="off" required="">
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                              
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="{{ __('auth.password') }}" name="password" required="">
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="{{ __('auth.confirm_password') }}" name="password_confirmation" required="">
            
                                </div>
                                
                                <div class="m-login__form-action">
                                    
                                    <button  type="submit" class="btn m-btn--pill m-btn--air  btn-primary btn-lg m-btn m-btn--custom">{{ __('auth.password_reset') }}</button>
                                </div>
                            </form>
                        </div>


                    
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ URL::to('/') }}/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="{{ URL::to('/') }}/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

        <!--end::Global Theme Bundle -->

        <!--begin::Page Scripts -->
        <script src="{{ URL::to('/') }}/assets/snippets/custom/pages/user/login.js" type="text/javascript"></script>
        <script type="text/javascript" src="{{ URL::to('/') }}/js/parsley/parsley.min.js"></script>
        <script type="text/javascript" src="{{ URL::to('/') }}/js/parsley/en.js"></script>
    </body>

    <!-- end::Body -->
</html>