<!DOCTYPE html>

<html lang="en">

    <!-- begin::Head -->
   <head>
    <meta charset="utf-8" />
    <title>{{__('info.site_name')}} - {{__('auth.login')}}</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
      });
    </script>

    <link href="{{ URL::to('/') }}/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('/') }}/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('/') }}/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('/') }}/css/main.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{ URL::to('/') }}/images/system/icon.png" />
    <link rel="stylesheet" type="text/css" href="{{ URL::to('/') }}/css/parsley/parsley.css"/>
</head>

    <!-- end::Head -->

    <!-- begin::Body -->
    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-2" id="m_login" >
                <div class="m-grid__item m-grid__item--fluid    m-login__wrapper">
                    <div class="m-login__container">
                        <div class="m-login__logo">
                            <a href="/">
                                <img src="{{ URL::to('/') }}/images/system/shkolo.png">
                            </a>
                        </div>
                        @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                        @endif
                        <div class="m-login__signin">

                            <div class="m-login__head">
                                <h3 class="m-login__title">{{ __('auth.login') }}</h3>
                            </div>
                            <form class="m-login__form m-form" action="{{ route('login') }}" method="POST" data-parsley-validate>
                                @csrf
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" placeholder="{{ __('auth.email') }}" name="email" autocomplete="off" required>
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" placeholder="{{ __('auth.password') }}" name="password" required>
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="row m-login__form-sub">
                                    <div class="col m--align-left m-login__form-left">
                                        <label class="m-checkbox  m-checkbox--primary">
                                            <input type="checkbox" name="remember" id="remember"> {{ __('auth.remember_me') }}
                                            <span></span>
                                        </label>
                                    </div>
                                    
                                </div>
                                <div class="m-login__form-action">
                                    
                                    <button  class="btn m-btn--pill m-btn--air  btn-primary btn-lg m-btn m-btn--custom">{{ __('auth.login') }}</button>
                                </div>
                            </form>
                        </div>

                        <div class="m-login__forget-password">

                            <div class="m-login__head">
                                <h3 class="m-login__title">{{ __('auth.forget_password') }}</h3>
                                <div class="m-login__desc">{{ __('auth.enter_email') }}</div>
                            </div>
                            <form class="m-login__form m-form" action="{{ route('password.email') }}" method="POST" data-parsley-validate>
                                @csrf
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="email" placeholder="{{ __('auth.email') }}" name="email" id="m_email" autocomplete="off" required="">
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <br>
                                <div class="m-login__form-action">
                                    <button   class="btn m-btn--pill m-btn--air         btn-primary btn-lg m-btn m-btn--custom">{{ __('auth.password_reset') }}</button>&nbsp;&nbsp;
                                    <button type="button" id="m_login_forget_password_cancel" class="btn m-btn--pill m-btn--air         btn-outline-primary btn-lg m-btn m-btn--custom">{{ __('auth.cancel') }}</button>
                                    
                                </div>
                            </form>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Page -->

        <!--begin::Global Theme Bundle -->
        <script src="{{ URL::to('/') }}/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="{{ URL::to('/') }}/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

        <!--end::Global Theme Bundle -->

        <!--begin::Page Scripts -->
        <script src="{{ URL::to('/') }}/assets/snippets/custom/pages/user/login.js" type="text/javascript"></script>
        <script type="text/javascript" src="{{ URL::to('/') }}/js/parsley/parsley.min.js"></script>
        <script type="text/javascript" src="{{ URL::to('/') }}/js/parsley/en.js"></script>

        <!--end::Page Scripts -->
    </body>

    <!-- end::Body -->
</html>