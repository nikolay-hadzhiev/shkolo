<!DOCTYPE html>
<html lang="bg">
   
        
     @include('partials._head')
   
     @include('partials._messages')

     @include('partials._navigation')

     @include('partials._aside')

     @yield('content')

     @include('partials._footer')

     
           
</html>
