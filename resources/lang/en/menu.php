<?php

return [

	'dashboard' => 'Dashboard',
	'home' => 'Home',
	'menu' => 'Menu',
    'users' => 'Users',
    'properties' => 'Properties',
    'permissions' => 'Permissions',
    'logs' => 'Logs',
    'labels' => 'Labels',
    'pages' => 'Pages',
    'languages' => 'Languages',
    'beginning' => 'Beginning',
    'video' => 'Video',
    'tixi' => 'Why Tixi?',
    'faq' => 'FAQ',
    'reviews' => 'Reviews',
    'download' => 'Download',
    'footer' => 'Footer',
    'headers' => 'Headers',

];
