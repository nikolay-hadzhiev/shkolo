<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'logout'  => 'Logout',
    'login'  => 'Sign in',
    'login_system'  => 'Sign in',
    'forget_password' => 'Forget Password ?',
    'email' => 'Email',
    'password' => 'Password',
    'confirm_password' => 'Confirm password',
    'forget_password_email' => 'Forget password email',
    'remember_me' => 'Remember me',
    'password_recovery' => 'Password recovery',
    'password_reset' => 'Reset password',
    'password_confirmation' => 'Password confirmation',
    'password_create' => 'Create password',
    'enter_email' => 'Enter your email to reset your password:',
    'cancel' => 'Cancel',

];
