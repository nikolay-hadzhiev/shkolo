<?php
return [

    'confirm_delete' => 'Do you want to delete selected record?',
    'insert_success' => 'Insert success!',
    'delete_success' => 'Delete success!',
    'update_success' => 'Update success!',
    'no_permission'  => "You don't have permissions!",
    'upload_success' => 'Upload success!',
    'upload_error'   => 'Upload error!',
    'page_not_found' => 'Page not found!',
    'select_recipients' => 'Select recipients!',
    'send_success' => 'Send success!',
    'system_message' => 'System message',
    'page_not_found_message' => 'Page not found'
   

];