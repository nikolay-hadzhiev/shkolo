<?php

return [

    'dashboard' => 'Dashboard',
    'edit_tab' => 'Edit tab',
    'create_tab' => 'Create tab',
    'title' => 'Title',
    'link' => 'Link',
    'color' => 'Color',
    'colors' =>[
    	'blue' => 'Blue',
    	'green' => 'Green',
    	'red' => 'Red',
    	'yellow' => 'Yellow',
    	'black' => 'Black',
    	'white' => 'White',
    ],
    'new_tab' => 'New Tab'

];
