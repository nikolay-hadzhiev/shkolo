<?php
return [

     'back'  => 'Back',
     'print' => 'Print',
     'add' => 'Add',
     'edit' => 'Edit',
     'save' => 'Save',
     'delete' => 'Delete',
     'yes' => 'Yes',
     'no' => 'No',
     'create' => 'Create',
];