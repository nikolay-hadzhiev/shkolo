<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

	Route::get('/', 'DashboardController@index');
	Route::get('/dashboard', 'DashboardController@index');
	Route::get('/dashboard/edit/{id}', 'DashboardController@edit');
	Route::post('/dashboard/store', 'DashboardController@store');
	Route::post('/dashboard/update/{id}', 'DashboardController@update');
	Route::get('/dashboard/create', 'DashboardController@create');
	Route::delete('/dashboard/delete/{id}', 'DashboardController@destroy');

});

Auth::routes();



